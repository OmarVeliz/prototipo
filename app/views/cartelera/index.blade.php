<!DOCTYPE html>
<html>
<head>
    <title>Agregar cartelera</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<h1>Todas las carteleras</h1>
{{ HTML::link(URL::to('cartelera/create'), 'Cartelera') }}
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <td>Sala</td>
        <td>Pelicula</td>
        <td>Formato</td>
        <td>Lenguaje</td>
        <td>Fecha</td>
        <td>Hora</td>
    </tr>
    </thead>
    <tbody>
    @foreach($Cartelera as $key => $value)
        <tr>
            <td>{{ $value->sala_id }}</td>
            <td>{{ $value->pelicula_id }}</td>
            <td>{{ $value->formatopelicula_id }}</td>
            <td>{{ $value->formato_lenguaje }}</td>
            <td>{{ $value->fecha }}</td>
            <td>{{ $value->hora }}</td>
            <td>
                {{ Form::open(array('url' => 'cartelera/' . $value->id, 'class' => 'pull-right')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('cartelera/' . $value->id) }}">Show</a>

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('cartelera/' . $value->id . '/edit') }}">Edit</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<ul class="nav nav-pills">
    <li class="active">
        <a class="navbar-brand" href="{{ URL::to('cines') }}">Cines</a>
    </li>
    <li>
        <a class="navbar-brand" href="{{ URL::to('cartelera') }}">cartelera</a>
    </li>
    <li class="disabled">
        <a class="navbar-brand" href="{{ URL::to('formato') }}">formato</a>
    </li>
    <li class="disabled">
        <a class="navbar-brand" href="{{ URL::to('peliculas') }}">peliculas</a>
    </li>
    <li class="disabled">
        <a class="navbar-brand" href="{{ URL::to('salas') }}">salas</a>
    </li>
    <li class="disabled">
        <a class="navbar-brand" href="{{ URL::to('tipo') }}">tipo</a>
    </li>
    <li class="dropdown pull-right">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle">Dropdown<strong class="caret"></strong></a>
        <ul class="dropdown-menu">
            <li>
                <a class="navbar-brand" href="{{ URL::to('cines') }}">Cines</a>>
            </li>
            <li>
                <a href="#">Another action</a>
            </li>
            <li>
                <a href="#">Something else here</a>
            </li>
            <li class="divider">
            </li>
            <li>
                <a href="#">Separated link</a>
            </li>
        </ul>
    </li>
</ul>

</body>
</html>