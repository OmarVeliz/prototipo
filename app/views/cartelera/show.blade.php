<!DOCTYPE html>
<html>
<head>
    <title>Mostrar carteleras</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <h1>{{ $cartelera_detail->formato_lenguaje }}</h1>

        <div class="jumbotron text-left">
            <h2>{{$cartelera_detail->formato_lenguaje }}</h2>
            <p>
                <strong>Fecha:</strong> {{ $cartelera_detail->fecha }}<br>
            </p>
        </div>

    </div>
    </body>
</html>