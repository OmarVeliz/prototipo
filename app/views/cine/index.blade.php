<!DOCTYPE html>
<html>
<head>
    <title>Ver cine</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<h1>Cines:</h1>
{{ HTML::link(URL::to('cines/create'), 'Agregar cine') }}
<hr>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <td>identificacion</td>
        <td>Nombre</td>
        <td>Direccion</td>
        <td>Telefono</td>
        <td>Latitud</td>
        <td>Longitud</td>
        <td>Hora apertura</td>
        <td>Hora cierre</td>
    </tr>
    </thead>
    <tbody>
    @foreach($Cine as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->nombre }}</td>
            <td>{{ $value->direccion }}</td>
            <td>{{ $value->telefono }}</td>
            <td>{{ $value->latitud }}</td>
            <td>{{ $value->longitud }}</td>
            <td>{{ $value->hora_apertura }}</td>
            <td>{{ $value->hora_cierre }}</td>
            <!-- we will also add show, edit, and delete buttons -->
            <td>
                {{ Form::open(array('url' => 'cines/' . $value->id, 'class' => 'pull-right')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('cines/' . $value->id) }}">Show</a>

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('cines/' . $value->id . '/edit') }}">Edit</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<hr>

<ul class="nav nav-pills">
    <li class="active">
        <a class="navbar-brand" href="{{ URL::to('cines') }}">Cines</a>
    </li>
    <li>
        <a class="navbar-brand" href="{{ URL::to('cartelera') }}">cartelera</a>
    </li>
    <li class="disabled">
        <a class="navbar-brand" href="{{ URL::to('formato') }}">formato</a>
    </li>
    <li class="disabled">
        <a class="navbar-brand" href="{{ URL::to('peliculas') }}">peliculas</a>
    </li>
    <li class="disabled">
        <a class="navbar-brand" href="{{ URL::to('salas') }}">salas</a>
    </li>
    <li class="disabled">
        <a class="navbar-brand" href="{{ URL::to('tipo') }}">tipo</a>
    </li>
    <li class="dropdown pull-right">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle">Dropdown<strong class="caret"></strong></a>
        <ul class="dropdown-menu">
            <li>
                <a class="navbar-brand" href="{{ URL::to('cines') }}">Cines</a>>
            </li>
            <li>
                <a href="#">Another action</a>
            </li>
            <li>
                <a href="#">Something else here</a>
            </li>
            <li class="divider">
            </li>
            <li>
                <a href="#">Separated link</a>
            </li>
        </ul>
    </li>
</ul>

</body>
</html>