<!DOCTYPE html>
<html>
<head>
    <title>Agregar nueva sala</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <h1>Agregar</h1>

        <!-- if there are creation errors, they will show here -->
        {{ HTML::ul($errors->all()) }}

        {{ Form::open(array('url' => 'salas')) }}

        <div class="form-group">
            {{ Form::label('cine_id', 'Cine') }}
            {{ Form::text('cine_id', Input::old('cine_id'), array('class' => 'form-control')) }}
            {{ Form::label('numero', 'Numero') }}
            {{ Form::text('numero', Input::old('numero'), array('class' => 'form-control')) }}
            {{ Form::label('tiposala_id', 'Tipo sala') }}
            {{ Form::text('tiposala_id', Input::old('tiposala_id'), array('class' => 'form-control')) }}
        </div>

        {{ Form::submit('Agregar sala!', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}

    </div>
    </body>
</html>