<!DOCTYPE html>
<html>
<head>
    <title>Mostrar salas</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <h1> {{ $sala_detail->numero }}</h1>

        <div class="jumbotron text-center">
            <h2>{{$sala_detail->numero }}</h2>
            <p>
                <strong>Tipo:</strong> {{ $sala_detail->tiposala_id }}<br>
            </p>
        </div>

    </div>
    </body>
</html>