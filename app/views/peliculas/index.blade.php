<!DOCTYPE html>
<html>
<head>
    <title>Mostrar</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<h1>Todas las peliculas</h1>
    {{ HTML::link(URL::to('peliculas/create'), 'Agregar pelicula') }}
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>id</td>
            <td>Titulo</td>
            <td>Sinopsis</td>
            <td>Trailer</td>
            <td>Image</td>
            <td>Rated</td>
            <td>Genero</td>
        </tr>
        </thead>
        <tbody>
        @foreach($Pelicula as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->titulo }}</td>
                <td>{{ $value->sinopsis }}</td>
                <td>
                    {{$value->trailer_url}}
                </td>
                <td>
                    <img src="{{ $value -> image}}" width="200" height="200"/>
                </td>
                <td>{{ $value->rated }}</td>
                <td>{{ $value->genero }}</td>
                <!-- we will also add show, edit, and delete buttons -->
                <td>

                    {{ Form::open(array('url' => 'peliculas/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                    {{ Form::close() }}
                    <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                    <a class="btn btn-small btn-success" href="{{ URL::to('peliculas/' . $value->id) }}">Show</a>

                    <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                    <a class="btn btn-small btn-info" href="{{ URL::to('peliculas/' . $value->id . '/edit') }}">Edit</a>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

<ul class="nav nav-pills">
    <li class="active">
        <a class="navbar-brand" href="{{ URL::to('cines') }}">Cines</a>
    </li>
    <li>
        <a class="navbar-brand" href="{{ URL::to('cartelera') }}">cartelera</a>
    </li>
    <li class="disabled">
        <a class="navbar-brand" href="{{ URL::to('formato') }}">formato</a>
    </li>
    <li class="disabled">
        <a class="navbar-brand" href="{{ URL::to('peliculas') }}">peliculas</a>
    </li>
    <li class="disabled">
        <a class="navbar-brand" href="{{ URL::to('salas') }}">salas</a>
    </li>
    <li class="disabled">
        <a class="navbar-brand" href="{{ URL::to('tipo') }}">tipo</a>
    </li>
    <li class="dropdown pull-right">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle">Dropdown<strong class="caret"></strong></a>
        <ul class="dropdown-menu">
            <li>
                <a class="navbar-brand" href="{{ URL::to('cines') }}">Cines</a>>
            </li>
            <li>
                <a href="#">Another action</a>
            </li>
            <li>
                <a href="#">Something else here</a>
            </li>
            <li class="divider">
            </li>
            <li>
                <a href="#">Separated link</a>
            </li>
        </ul>
    </li>
</ul>

</body>
</html>