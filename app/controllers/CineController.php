<?php

class CineController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        $nerds = Cine::all();

        return View::make('cine.index')
            ->with('Cine', $nerds);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('cine.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //

        $rules = array(
            'nombre' => 'required',
            'direccion' => 'required',
            'telefono' => 'required',
            'latitud' => 'required',
            'longitud' => 'required',
            'hora_apertura' => 'required',
            'hora_cierre' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('cines/create')
                ->withErrors($validator);
        } else {
            // store
            $nerd = new Cine();
            $nerd->nombre = Input::get('nombre');
            $nerd->direccion = Input::get('direccion');
            $nerd->telefono = Input::get('telefono');
            $nerd->latitud = Input::get('latitud');
            $nerd->longitud = Input::get('longitud');
            $nerd->hora_apertura = Input::get('hora_apertura');
            $nerd->hora_cierre = Input::get('hora_cierre');
            $nerd->save();

            return Redirect::to('cines');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $nerd = Cine::find($id);

        // show the view and pass the nerd to it
        return View::make('cine.show')
            ->with('cine_detail', $nerd);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $nerd = Cine::find($id);

        // show the edit form and pass the nerd
        return View::make('cine.edit')
            ->with('cine_detail', $nerd);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
        $rules = array(
            'nombre' => 'required',
            'direccion' => 'required',
            'telefono' => 'required',
            'latitud' => 'required',
            'longitud' => 'required',
            'hora_apertura' => 'required',
            'hora_cierre' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('cines/' . $id . '/edit')
                ->withErrors($validator);
        } else {
            // store
            $nerd = Cine::find($id);
            $nerd->nombre = Input::get('nombre');
            $nerd->direccion = Input::get('direccion');
            $nerd->telefono = Input::get('telefono');
            $nerd->latitud = Input::get('latitud');
            $nerd->longitud = Input::get('longitud');
            $nerd->hora_apertura = Input::get('hora_apertura');
            $nerd->hora_cierre = Input::get('hora_cierre');
            $nerd->save();

            return Redirect::to('cines');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $nerd = Cine::find($id);
        $nerd->delete();

        return Redirect::to('cines');
    }


}
